package com.example.thanhhoang.engmatesting.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thanhhoang.engmatesting.Model.PhotoModel;
import com.example.thanhhoang.engmatesting.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PhotoVerticalAdapter extends RecyclerView.Adapter<PhotoVerticalAdapter.ViewHolder> {
    private List<PhotoModel> photos;

    public void setPhotos(List<PhotoModel> photos){
        this.photos = photos;
    }

    @NonNull
    @Override
    public PhotoVerticalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_vertical, parent, false);
        PhotoVerticalAdapter.ViewHolder viewHolder = new PhotoVerticalAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoVerticalAdapter.ViewHolder holder, int position) {
        PhotoModel photo = photos.get(position);
        holder.tvTitle.setText(photo.getTitle());
        List<PhotoModel.SizeModel> sizes = photo.getSizes();
        if (sizes != null && sizes.size() > 0){
            String urlImage = sizes.get(0).getSource();
            Picasso.get()
                    .load(urlImage)
                    .into(holder.ivPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return photos == null ? 0 : photos.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTitle;
        public ImageView ivPhoto;

        public ViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            ivPhoto = view.findViewById(R.id.ivPhoto);
        }
    }

}