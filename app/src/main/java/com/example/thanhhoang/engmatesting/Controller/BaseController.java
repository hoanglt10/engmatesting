package com.example.thanhhoang.engmatesting.Controller;

import android.content.Context;

import com.example.thanhhoang.engmatesting.Model.BaseModel;


public abstract class BaseController<T extends BaseModel> {
    protected T model;
    protected Context context;

    public BaseController(Context context, T model){
        this.model = model;
        this.context = context;
    }
}
