package com.example.thanhhoang.engmatesting.Controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.thanhhoang.engmatesting.Interface.IRequest;
import com.example.thanhhoang.engmatesting.Interface.IView;
import com.example.thanhhoang.engmatesting.Model.PhotoModel;

import java.util.List;

public class PhotoController extends BaseController<PhotoModel> {
    private Context context;
    private List<PhotoModel> photosHorizontal;
    private List<PhotoModel> photosVertical;
//    private int index = 0;

    public PhotoController(Context context) {
        super(context, PhotoModel.newInstance());
        this.context = context;
    }

    public void getPhotosHorizontal(final IView iView, int itemPerPage, int currentPage){
        IRequest iRequest = new IRequest<List<PhotoModel>>() {
            @Override
            public void success(List<PhotoModel> photoModels) {
                photosHorizontal = photoModels;
                for(PhotoModel photo : photoModels) {
                    getSizesHorizontal(photo, iView);
                }
            }

            @Override
            public void error(VolleyError error) {

            }
        };

        model.requestData(
                context,
                Request.Method.GET,
                model.URL_PHOTO  + itemPerPage + "&page=" + currentPage,
                iRequest
        );
    }

    public void getSizesHorizontal(final PhotoModel photo, final IView iView){
        IRequest iRequest = new IRequest<List<PhotoModel.SizeModel>>() {
            @Override
            public void success(List<PhotoModel.SizeModel> sizes) {
                int index = photosHorizontal.indexOf(photo);
                PhotoModel model = photosHorizontal.get(index);
                model.setSizes(sizes);
                photosHorizontal.set(index, photo);
                if (index == photosHorizontal.size() - 1) {
                    iView.updateUI(photosHorizontal);
                }
            }

            @Override
            public void error(VolleyError error) {

            }
        };

        new PhotoModel.SizeModel().requestData(
                context,
                Request.Method.GET,
                model.URL_SIZE + photo.getId(),
                iRequest
        );
    }



    public void getPhotosVertical(final IView iView, int itemPerPage, int currentPage){
        IRequest iRequest = new IRequest<List<PhotoModel>>() {
            @Override
            public void success(List<PhotoModel> photoModels) {
                photosVertical = photoModels;
                for(PhotoModel photo : photoModels) {
                    getSizesVertical(photo, iView);
                }
            }

            @Override
            public void error(VolleyError error) {

            }
        };

        model.requestData(
                context,
                Request.Method.GET,
                model.URL_PHOTO  + itemPerPage + "&page=" + currentPage,
                iRequest
        );
    }

    public void getSizesVertical(final PhotoModel photo, final IView iView){
        IRequest iRequest = new IRequest<List<PhotoModel.SizeModel>>() {
            @Override
            public void success(List<PhotoModel.SizeModel> sizes) {
                int index = photosVertical.indexOf(photo);
                PhotoModel model = photosVertical.get(index);
                model.setSizes(sizes);
                photosVertical.set(index, photo);
                if (index == photosVertical.size() - 1) {
                    iView.updateUI(photosVertical);
                }
            }

            @Override
            public void error(VolleyError error) {

            }
        };

        new PhotoModel.SizeModel().requestData(
                context,
                Request.Method.GET,
                model.URL_SIZE + photo.getId(),
                iRequest
        );
    }

}
