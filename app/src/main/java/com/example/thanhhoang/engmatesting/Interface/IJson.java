package com.example.thanhhoang.engmatesting.Interface;

public interface IJson<T> {
    T parseJsonObject();
}
