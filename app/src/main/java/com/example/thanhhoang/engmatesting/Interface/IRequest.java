package com.example.thanhhoang.engmatesting.Interface;

import com.android.volley.VolleyError;

public interface IRequest<T> {
    void success(T t);
    void error(VolleyError error);
}
