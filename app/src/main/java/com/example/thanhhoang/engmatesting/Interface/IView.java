package com.example.thanhhoang.engmatesting.Interface;

public interface IView<T> {
    void updateUI(T t);
}
