package com.example.thanhhoang.engmatesting.Model;

import android.content.Context;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.thanhhoang.engmatesting.Helper.MyLog;
import com.example.thanhhoang.engmatesting.Interface.IJson;
import com.example.thanhhoang.engmatesting.Interface.IRequest;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;


public abstract class BaseModel{
    abstract void parseJsonObject(String jsonString, IRequest iRequest);

    public void requestData(Context context, int method, final String url, final IRequest iRequest){
        RequestQueue queue = Volley.newRequestQueue(context);
        Request request = new StringRequest(
                method,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String log = url + "\n" + response;
                        MyLog.showLog(log);
                        if (iRequest != null){
                            parseJsonObject(response, iRequest);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyLog.showLog(error.toString());
                        if (iRequest != null){
                            iRequest.error(error);
                        }
                    }
                }
        );
        queue.add(request);
    }

}
