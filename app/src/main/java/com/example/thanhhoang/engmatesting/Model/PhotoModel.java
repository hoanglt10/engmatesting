package com.example.thanhhoang.engmatesting.Model;

import android.os.Bundle;

import com.android.volley.VolleyError;
import com.example.thanhhoang.engmatesting.Interface.IRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PhotoModel extends BaseModel {
    public static final String URL_PHOTO = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=22f5ef6f65e96129e59a970453527bf6&photoset_id=72157666537470427&format=json&nojsoncallback=1&per_page=";
    public static final String URL_SIZE = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=22f5ef6f65e96129e59a970453527bf6&format=json&nojsoncallback=1&photo_id=";

    private static PhotoModel model;
    private String id;
    private String secret;
    private int server;
    private int farm;
    private String title;
    private List<SizeModel> sizes;


    public static PhotoModel newInstance() {
        if (model == null){
            model = new PhotoModel();
        }
        return model;
    }

    public String getId() {
        return id;
    }

    public String getSecret() {
        return secret;
    }

    public int getServer() {
        return server;
    }

    public int getFarm() {
        return farm;
    }

    public String getTitle() {
        return title;
    }

    public List<SizeModel> getSizes() {
        return sizes;
    }

    public void setSizes(List<SizeModel> sizes) {
        this.sizes = sizes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    void parseJsonObject(String response, IRequest iRequest) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject photoSet = jsonObject.getJSONObject("photoset");
            JSONArray photo = photoSet.getJSONArray("photo");
            if (photo != null){
                List<PhotoModel> photos = new Gson().fromJson(photo.toString(), new TypeToken<List<PhotoModel>>(){}.getType());
                iRequest.success(photos);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static class SizeModel extends BaseModel{
        private String label;
        private int width;
        private int height;
        private String source;
        private String url;
        private String media;

        public String getLabel() {
            return label;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public String getSource() {
            return source;
        }

        public String getUrl() {
            return url;
        }

        public String getMedia() {
            return media;
        }

        @Override
        public String toString() {
            return "Size{" +
                    "label='" + label + '\'' +
                    ", width=" + width +
                    ", height=" + height +
                    ", source='" + source + '\'' +
                    ", url='" + url + '\'' +
                    ", media='" + media + '\'' +
                    '}';
        }

        @Override
        void parseJsonObject(String response, IRequest iRequest) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject sizeObject = jsonObject.getJSONObject("sizes");
                JSONArray sizeArray = sizeObject.getJSONArray("size");
                if (sizeArray != null){
                    List<PhotoModel.SizeModel> photos = new Gson().fromJson(sizeArray.toString(), new TypeToken<List<PhotoModel.SizeModel>>(){}.getType());
                    iRequest.success(photos);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
