package com.example.thanhhoang.engmatesting.View;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.thanhhoang.engmatesting.Controller.BaseController;


public abstract class BaseActivity extends AppCompatActivity {
    private int layoutId;
    protected Context context;

    protected abstract void onInitView();
    protected abstract void setData();

    protected BaseActivity(int layoutId){
        this.layoutId = layoutId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId);
        this.context = BaseActivity.this;
        onInitView();
        setData();
    }
}
