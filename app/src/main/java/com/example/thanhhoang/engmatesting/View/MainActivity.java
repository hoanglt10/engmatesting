package com.example.thanhhoang.engmatesting.View;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.thanhhoang.engmatesting.Adapter.PhotoHorizontalAdapter;
import com.example.thanhhoang.engmatesting.Adapter.PhotoVerticalAdapter;
import com.example.thanhhoang.engmatesting.Controller.PhotoController;
import com.example.thanhhoang.engmatesting.Helper.MyLog;
import com.example.thanhhoang.engmatesting.Interface.IView;
import com.example.thanhhoang.engmatesting.Model.PhotoModel;
import com.example.thanhhoang.engmatesting.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends  BaseActivity{

    private int itemPerPage = 12;
    private int currentPageHorizontal = 1, currentPageVertical = 1;
    private RecyclerView rvPhotoHorizontal, rvPhotoVertical;
    private PhotoHorizontalAdapter adapterHorizontal;
    private PhotoVerticalAdapter adapterVertical;
    private LinearLayoutManager layoutManagerHorizontal;
    private GridLayoutManager layoutManagerVertical;
    private boolean isLoadingHorizontal = true, isLoadingVertical = true;
    private List<PhotoModel> photosHorizontal = new ArrayList<>();
    private List<PhotoModel> photosVertical = new ArrayList<>();

    protected MainActivity() {
        super(R.layout.activity_main);
    }

    @Override
    protected void onInitView() {
        rvPhotoHorizontal = findViewById(R.id.rvPhotoHorizontal);
        rvPhotoVertical = findViewById(R.id.rvPhotoVertical);
    }

    @Override
    protected void setData() {
        setPhotoHorizontalAdapter();
        setPhotoVerticalAdapter();
        getPhotosHorizontal();
        getPhotosVertical();
    }

    private void getPhotosHorizontal(){
        new PhotoController(context).getPhotosHorizontal(new IView<List<PhotoModel>>() {
            @Override
            public void updateUI(List<PhotoModel> photoModels) {
                if (photoModels == null){
                    return;
                }
                photosHorizontal.addAll(photoModels);
                adapterHorizontal.setPhotos(photosHorizontal);
                adapterHorizontal.notifyDataSetChanged();
                if (photoModels.size() >= itemPerPage) {
                    isLoadingHorizontal = false;
                }
            }
        }, itemPerPage, currentPageHorizontal);
    }

    private void getPhotosVertical(){
        new PhotoController(context).getPhotosVertical(new IView<List<PhotoModel>>() {
            @Override
            public void updateUI(List<PhotoModel> photoModels) {
                if (photoModels == null){
                    return;
                }
                photosVertical.addAll(photoModels);
                adapterVertical.setPhotos(photosVertical);
                adapterVertical.notifyDataSetChanged();
                if (photoModels.size() >= itemPerPage) {
                    isLoadingVertical = false;
                }
            }
        }, itemPerPage, currentPageVertical);
    }

    private void setPhotoHorizontalAdapter(){
        adapterHorizontal = new PhotoHorizontalAdapter();
        adapterHorizontal.setPhotos(photosHorizontal);
        layoutManagerHorizontal = new LinearLayoutManager(this);
        layoutManagerHorizontal.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPhotoHorizontal.setAdapter(adapterHorizontal);
        rvPhotoHorizontal.setLayoutManager(layoutManagerHorizontal);
        rvPhotoHorizontal.addOnScrollListener(onScrollListener);
    }

    private void setPhotoVerticalAdapter(){
        adapterVertical = new PhotoVerticalAdapter();
        adapterVertical.setPhotos(photosHorizontal);
        layoutManagerVertical = new GridLayoutManager(this, 3);
        layoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        rvPhotoVertical.setAdapter(adapterVertical);
        rvPhotoVertical.setLayoutManager(layoutManagerVertical);
        rvPhotoVertical.addOnScrollListener(onScrollListener);
    }


    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            MyLog.showLog("SCROLL "  + newState + "");
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            MyLog.showLog("SCROLL "  + dx + " - " + dy);
            switch (recyclerView.getId()){
                case R.id.rvPhotoHorizontal: {
                    if (isLoadingHorizontal)
                        return;
                    int visibleItemCount = layoutManagerHorizontal.getChildCount();
                    int totalItemCount = layoutManagerHorizontal.getItemCount();
                    int pastVisibleItems = layoutManagerHorizontal.findFirstVisibleItemPosition();
                    if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                        isLoadingHorizontal = true;
                        currentPageHorizontal ++;
                        getPhotosHorizontal();
                        Toast.makeText(context, "IsLoadingHorizontal Page " + currentPageHorizontal, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case R.id.rvPhotoVertical: {
                    if (isLoadingVertical)
                        return;
                    int visibleItemCount = layoutManagerVertical.getChildCount();
                    int totalItemCount = layoutManagerVertical.getItemCount();
                    int pastVisibleItems = layoutManagerVertical.findFirstVisibleItemPosition();
                    if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                        isLoadingVertical = true;
                        currentPageVertical++;
                        getPhotosVertical();
                        Toast.makeText(context, "IsLoadingVertical Page " + currentPageVertical, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }
        }
    };

}
